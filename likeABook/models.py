from django.db import models

# Create your models here.


class Book(models.Model):
    id = models.CharField(max_length=300, primary_key=True, unique=True)
    title = models.CharField(max_length=300)
    author = models.CharField(max_length=300)
    thumbnail = models.CharField(max_length=300)
    categories = models.CharField(max_length=300)
    publisher = models.CharField(max_length=300)
    likeCnt = models.IntegerField(default=0)

    class Meta:
        ordering = ('-likeCnt',)

    def __str__(self):
        return self.title

    objects = models.Manager()
