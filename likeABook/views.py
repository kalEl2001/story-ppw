from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from .models import *
import json


@csrf_exempt
def likeABook(request):
    if(request.method == "POST"):
        books = list(Book.objects.values()[:5])
        return HttpResponse(json.dumps(books))
    else:
        user = request.user
        context = {
            'user': user,
        }
        return render(request, 'likeABook.html', context)


@csrf_exempt
def like(request):
    if(request.method == "POST"):
        id = request.POST['id']
        title = request.POST['title']
        author = request.POST['author']
        thumbnail = request.POST['thumbnail']
        categories = request.POST['categories']
        publisher = request.POST['publisher']
        book, created = Book.objects.get_or_create(id=id, title=title, author=author, thumbnail=thumbnail,
                                                   categories=categories, publisher=publisher)
        book.likeCnt += 1
        book.save()
        return HttpResponse(book.likeCnt)


@csrf_exempt
def dislike(request):
    if(request.method == "POST"):
        id = request.POST['id']
        title = request.POST['title']
        author = request.POST['author']
        thumbnail = request.POST['thumbnail']
        categories = request.POST['categories']
        publisher = request.POST['publisher']
        book = Book.objects.get(id=id, title=title, author=author, thumbnail=thumbnail,
                                categories=categories, publisher=publisher)
        book.likeCnt -= 1
        book.save()
        return HttpResponse(book.likeCnt)
