from django.test import TestCase

# Create your tests here.
from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver import DesiredCapabilities
from selenium.webdriver.chrome.options import Options

from .models import *
from .views import *

import time
import os


class Story9UnitTests(TestCase):

    def test_likeABook_page_url_is_exist(self):
        response = Client().get('/books/')
        self.assertEqual(response.status_code, 200)

    def test_likeABook_page_is_using_likeABook_function(self):
        found = resolve('/books/')
        self.assertEqual(found.func, likeABook)

    def test_likeABook_page_is_using_correct_html(self):
        response = Client().get('/books/')
        self.assertTemplateUsed(response, 'likeABook.html')

    def test_likeABook_post_method(self):
        Client().post('/books/')

    def test_like_page_url_is_exist(self):
        response = Client().post('/books/like/',
                                 {'id': 'FmyBAwAAQBAJ', 'title': 'Sapiens', 'author': 'Yuval Noah Harari', 'thumbnail': 'http://books.google.com/books/content?id=FmyBAwAAQBAJ&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api', 'categories': 'Science', 'publisher': 'Harper Collins'})
        self.assertEqual(response.status_code, 200)

    def test_dislike_page_url_is_exist(self):
        Client().post('/books/like/',
                      {'id': 'FmyBAwAAQBAJ', 'title': 'Sapiens', 'author': 'Yuval Noah Harari', 'thumbnail': 'http://books.google.com/books/content?id=FmyBAwAAQBAJ&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api', 'categories': 'Science', 'publisher': 'Harper Collins'})
        response = Client().post('/books/dislike/',
                                 {'id': 'FmyBAwAAQBAJ', 'title': 'Sapiens', 'author': 'Yuval Noah Harari', 'thumbnail': 'http://books.google.com/books/content?id=FmyBAwAAQBAJ&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api', 'categories': 'Science', 'publisher': 'Harper Collins'})
        self.assertEqual(response.status_code, 200)

    def test_like_page_is_using_like_function(self):
        found = resolve('/books/like/')
        self.assertEqual(found.func, like)

    def test_dislike_page_is_using_dislike_function(self):
        found = resolve('/books/dislike/')
        self.assertEqual(found.func, dislike)

    def test_book_model(self):
        Book.objects.create(
            id='FmyBAwAAQBAJ', title='Sapiens', author='Yuval Noah Harari', thumbnail='http://books.google.com/books/content?id=FmyBAwAAQBAJ&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api', categories='Science', publisher='Harper Collins')
        count = Book.objects.all().count()
        self.assertEqual(count, 1)
        book = Book.objects.get(id='FmyBAwAAQBAJ')
        self.assertEqual(str(book), 'Sapiens')


class Story9FuncTests(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.selenium = webdriver.Chrome(
            './chromedriver', chrome_options=chrome_options)
        self.selenium.get(self.live_server_url + '/books/')
        super(Story9FuncTests, self).setUp()

    def test_search_a_book_and_like_and_dislike_the_first_one(self):
        selenium = self.selenium

        selenium.implicitly_wait(5)

        searchBox = selenium.find_element_by_id('query')
        searchBtn = selenium.find_element_by_id('search-button')
        searchBox.send_keys('Yuval')
        searchBtn.click()

        selenium.implicitly_wait(5)

        likeButtons = selenium.find_element_by_class_name('fa-heart-o')
        likeButtons.click()
        likeButtons.click()

    def test_top_books_modal(self):
        selenium = self.selenium

        selenium.implicitly_wait(5)

        topBooksBtn = selenium.find_element_by_id('top-rated-btn')
        topBooksBtn.click()

        time.sleep(2)

    def tearDown(self):
        self.selenium.quit()
        super(Story9FuncTests, self).tearDown()
