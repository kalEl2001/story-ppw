from django.urls import path
from .views import *

urlpatterns = [
    path('', likeABook, name='likeABook'),
    path('like/', like, name='like'),
    path('dislike/', dislike, name='dislike'),
]
