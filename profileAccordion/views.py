from django.shortcuts import render


def profileAccordion(request):
    return render(request, 'profileAccordion.html')
