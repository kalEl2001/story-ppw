from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver import DesiredCapabilities
from selenium.webdriver.chrome.options import Options

from .views import *

import time
import os


class Story8UnitTests(TestCase):

    def test_profile_page_url_is_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)

    def test_profile_page_is_using_index_function(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, profileAccordion)

    def test_profile_page_is_using_correct_html(self):
        response = Client().get('/profile/')
        self.assertTemplateUsed(response, 'profileAccordion.html')


class Story8FuncTests(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.selenium = webdriver.Chrome(
            './chromedriver', chrome_options=chrome_options)
        self.selenium.get(self.live_server_url + '/profile/')
        super(Story8FuncTests, self).setUp()

    def test_if_accordion_can_expand(self):
        selenium = self.selenium

        selenium.implicitly_wait(5)

        basicInfoAccordion = selenium.find_element_by_id(
            'accordion1')

        basicInfoAccordion.click()

        classes = basicInfoAccordion.get_attribute("class")

        accordionIsOpen = classes.find("active")

        self.assertNotEqual(accordionIsOpen, -1)

    def test_if_accordion_can_retract(self):
        selenium = self.selenium

        selenium.implicitly_wait(5)

        basicInfoAccordion = selenium.find_element_by_id(
            'accordion1')

        basicInfoAccordion.click()

        basicInfoAccordion.click()

        classes = basicInfoAccordion.get_attribute("class")

        accordionIsOpen = classes.find("active")

        self.assertEqual(accordionIsOpen, -1)

    def test_if_accordion_can_move_down(self):
        selenium = self.selenium

        selenium.implicitly_wait(5)

        downButtonAccordion1 = selenium.find_element_by_id(
            'down1')

        downButtonAccordion1.click()

        upButtonAccordion1IsDisplayed = selenium.find_element_by_id(
            'up1').value_of_css_property('display')

        self.assertNotEqual(upButtonAccordion1IsDisplayed, "None")

    def test_if_accordion_can_move_up(self):
        selenium = self.selenium

        selenium.implicitly_wait(5)

        upButtonAccordion4 = selenium.find_element_by_id(
            'up4')

        upButtonAccordion4.click()

        downButtonAccordion4IsDisplayed = selenium.find_element_by_id(
            'down4').value_of_css_property('display')

        self.assertNotEqual(downButtonAccordion4IsDisplayed, "None")

    def test_if_light_mode_works(self):
        selenium = self.selenium

        selenium.implicitly_wait(5)

        lightModeToggleSwitch = selenium.find_element_by_id(
            'light-mode-toggle')

        lightModeToggleSwitch.click()

        basicInfoAccordionClasses = selenium.find_element_by_id(
            'accordion1').get_attribute('class')

        print(basicInfoAccordionClasses)

        accordionIsInLightMode = basicInfoAccordionClasses.find(
            "light-mode-header")

        self.assertNotEqual(accordionIsInLightMode, -1)

    def tearDown(self):
        self.selenium.quit()
        super(Story8FuncTests, self).tearDown()
