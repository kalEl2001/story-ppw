from django.db import models


class Message(models.Model):

    senderName = models.CharField(max_length=120)
    message = models.TextField()
    dateCreated = models.DateTimeField(auto_now_add=True)
    color = models.CharField(max_length=8, default="#efefef")

    class Meta:
        ordering = ('-dateCreated',)
        verbose_name_plural = 'Messages'

    def __str__(self):
        return self.senderName
