from django.urls import path
from .views import *

urlpatterns = [
    path('', index, name='index'),
    path('confirmation/', confirmation, name='confirmation'),
    path('changecolor/<int:key>', change_color, name='change_color'),
]
