from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver import DesiredCapabilities
from selenium.webdriver.chrome.options import Options

from .models import *
from .views import *
from .forms import *

import time
import os


class Story7UnitTests(TestCase):

    def test_messagesForSamuel_page_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_messagesForSamuel_page_is_using_index_function(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_messagesForSamuel_page_is_using_correct_html(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_confirmation_page_url_is_exist(self):
        response = Client().post('/confirmation/',
                                 {'senderName': 'Test Sender', 'message': 'This is a message for testing purpose.'})
        self.assertEqual(response.status_code, 200)

    def test_confirmation_page_is_using_confirmation_function(self):
        found = resolve('/confirmation/')
        self.assertEqual(found.func, confirmation)

    def test_confirmation_page_is_using_correct_html(self):
        response = Client().post('/confirmation/',
                                 {'senderName': 'Test Sender', 'message': 'This is a message for testing purpose.'})
        self.assertTemplateUsed(response, 'confirmation.html')

    def test_message_model(self):
        Message.objects.create(
            senderName='Test Sender', message='This is a message for testing purpose.')
        count = Message.objects.all().count()
        self.assertEqual(count, 1)
        message = Message.objects.get(senderName='Test Sender')
        self.assertEqual(str(message), 'Test Sender')

    def test_form_validation(self):
        form = MessageForm(data={'senderName': '', 'message': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['senderName'],
            ["This field is required."]
        )
        self.assertEqual(
            form.errors['message'],
            ["This field is required."]
        )


class Story7FuncTests(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.selenium = webdriver.Chrome(
            './chromedriver', chrome_options=chrome_options)
        self.selenium.get(self.live_server_url + '/')
        super(Story7FuncTests, self).setUp()

    def test_add_and_check_new_message(self):
        selenium = self.selenium

        selenium.implicitly_wait(5)

        addButton = selenium.find_element_by_id(
            'add-message-button')
        addButton.click()

        senderName = selenium.find_element_by_id('id_senderName')
        message = selenium.find_element_by_id('id_message')

        senderName.send_keys('Test Sender Func Testing')
        message.send_keys(
            'This is a message for functionality testing purposes.')

        submit = selenium.find_element_by_id('submit-button')
        submit.click()
        selenium.implicitly_wait(5)

        confirm = selenium.find_element_by_id('confirm-button')
        confirm.click()

    def test_change_message_box_color(self):
        selenium = self.selenium

        selenium.implicitly_wait(5)

        addButton = selenium.find_element_by_id(
            'add-message-button')
        addButton.click()

        senderName = selenium.find_element_by_id('id_senderName')
        message = selenium.find_element_by_id('id_message')

        senderName.send_keys('Test Sender Func Testing')
        message.send_keys(
            'This is a message for functionality testing purposes.')

        submit = selenium.find_element_by_id('submit-button')
        submit.click()
        selenium.implicitly_wait(5)

        confirm = selenium.find_element_by_id('confirm-button')
        confirm.click()

        messageCardOriginalColor = selenium.find_elements_by_class_name(
            'card')[0].value_of_css_property('background-color')

        changeColorButtons = selenium.find_elements_by_id(
            'change-color-button')
        changeColorButtons[0].click()

        self.assertNotEqual(messageCardOriginalColor, selenium.find_elements_by_class_name(
            'card')[0].value_of_css_property('background-color'))

    def tearDown(self):
        self.selenium.quit()
        super(Story7FuncTests, self).tearDown()
