from django.shortcuts import render, get_object_or_404, redirect
from .models import *
from .forms import *
from django.db.models import Q
import random


def index(request):
    if(request.method == "POST"):
        print(request.POST)
        senderName = request.POST['senderName']
        message = request.POST['message']
        Message.objects.create(senderName=senderName,
                               message=message)
        form = MessageForm()
        user = request.user
        messages = Message.objects.all()
        count = Message.objects.all().count()
        context = {
            'messages': messages,
            'count': count,
            'form': form,
            'user': user,
        }
        return render(request, 'index.html', context)
    else:
        form = MessageForm()
        user = request.user
        messages = Message.objects.all()
        count = Message.objects.all().count()
        context = {
            'messages': messages,
            'count': count,
            'form': form,
            'user': user,
        }
        return render(request, 'index.html', context)


def confirmation(request):
    if(request.method == 'POST'):
        senderName = request.POST['senderName']
        message = request.POST['message']
        context = {
            'senderName': senderName,
            'message': message,
        }
        return render(request, 'confirmation.html', context)


def change_color(request, key):
    message = Message.objects.get(id=key)
    choice = ['a', 'b', 'c', 'd', 'e', 'f']
    new_color = random.choice(choice) + random.choice(choice)
    new_color = 3*new_color
    message.color = '#' + new_color
    message.save(force_update=True)
    return redirect('index')
