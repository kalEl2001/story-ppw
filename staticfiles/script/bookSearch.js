function bookSearch() {
    var search = document.getElementById('query').value
    document.getElementById('results').innerHTML = ""

    $.ajax({
        url: "https://www.googleapis.com/books/v1/volumes?q=" + search,
        dataType: "json",
        success: function (data) {
            results.innerHTML = ''
            console.log(data)
            for (i = 0; i < data.items.length; i++) {
                results.innerHTML += '<br><div class="card mx-auto" style="width: 80vw;"><div class="card-body"><span style="color: #000000ee;"><h5 class="card-title">' + data.items[i].volumeInfo.title + '</h5><h6>' + data.items[i].volumeInfo.authors + '</h6><span><div class="row"><div class="col-2 text-right">' + '<img src="' + data.items[i].volumeInfo.imageLinks.thumbnail + '"> </div>' + '<div class="col-10 text-left" style="color: black;"><table><tr><td>Category&nbsp;&nbsp;</td><td>' + data.items[i].volumeInfo.categories + '</td></tr><tr><td>Publisher</td><td>' + data.items[i].volumeInfo.publisher + '</td></tr><tr><tr><td>Google Books ID&nbsp;&nbsp;</td><td>' + data.items[i].id + '</td></tr><td><div class = heart><i class="fa fa-heart-o" aria-hidden="true" ></i> </div></td></tr></table></div></div>' + '</div></div>'
            }
            $('.heart').on('click', function () {
                if ($(this).children().hasClass('fa-heart-o')) {
                    $(this).children().removeClass('fa-heart-o');
                    $(this).children().addClass('fa-heart');
                    $.ajax({
                        type: "POST",
                        url: "http://localhost:8000/books/like/",
                        dataType: "json",
                        data: {
                            'id': $(this).parent().parent().parent().children().children()[5].innerHTML,
                            'title': $(this).parent().parent().parent().parent().parent().parent().parent().parent().children()[0].innerHTML,
                            'author': $(this).parent().parent().parent().parent().parent().parent().parent().parent().children()[1].innerHTML,
                            'thumbnail': $(this).parent().parent().parent().parent().parent().parent().children().children()[0].src,
                            'categories': $(this).parent().parent().parent().children().children()[1].innerHTML,
                            'publisher': $(this).parent().parent().parent().children().children()[3].innerHTML,
                        },
                    })
                } else {
                    $(this).children().removeClass('fa-heart');
                    $(this).children().addClass('fa-heart-o');
                    $.ajax({
                        type: "POST",
                        url: "http://localhost:8000/books/dislike/",
                        dataType: "json",
                        data: {
                            'id': $(this).parent().parent().parent().children().children()[5].innerHTML,
                            'title': $(this).parent().parent().parent().parent().parent().parent().parent().parent().children()[0].innerHTML,
                            'author': $(this).parent().parent().parent().parent().parent().parent().parent().parent().children()[1].innerHTML,
                            'thumbnail': $(this).parent().parent().parent().parent().parent().parent().children().children()[0].src,
                            'categories': $(this).parent().parent().parent().children().children()[1].innerHTML,
                            'publisher': $(this).parent().parent().parent().children().children()[3].innerHTML,
                        },
                    })
                }
            })
        },

        type: 'GET'
    });
}

document.getElementById('search-button').addEventListener('click', bookSearch, false)
$('#query').keypress(function (e) {
    var keycode = (e.keyCode ? e.keyCode : e.which);
    if (keycode == '13') {
        bookSearch();
    }
    event.stopPropagation();
});

function topBooks() {
    $.ajax({
        type: "POST",
        url: "http://localhost:8000/books/",
        success: function (results) {
            var likedBooks = JSON.parse(results);
            console.log(likedBooks)
            $('.modal-body').empty()
            for (i = 0; i < likedBooks.length; i++) {
                $('.modal-body').append(
                    `<br>
                    <div class="card mx-auto" style="width: 100%;">
                    <div class="card-body">
                    <span style="color: #000000ee;">
                    <h5 class="card-title">${likedBooks[i].title}</h5>
                    <h6>${likedBooks[i].author}</h6>
                    </span>
                    <div class="row">
                    <div class="col-2 text-right">
                    <img src="${likedBooks[i].thumbnail}"> 
                    </div>
                    <div class="col-10 text-left" style="color: black;">
                    <table>
                    <tr>
                    <td>Category&nbsp;&nbsp;</td><td>${likedBooks[i].categories}</td>
                    </tr>
                    <tr>
                    <td>Publisher</td><td>${likedBooks[i].publisher}</td>
                    </tr>
                    <tr>
                    <td>Google Books ID&nbsp;&nbsp;</td><td>${likedBooks[i].id}</td>
                    </tr>
                    <tr>
                    <td>Liked</td><td>${likedBooks[i].likeCnt} times</td>
                    </tr>
                    </table>
                    </div>
                    </div>
                    </div>
                    </div>`
                )
            }
        }
    });
}

document.getElementById('top-rated-btn').addEventListener('click', topBooks, false)
