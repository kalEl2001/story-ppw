$(".accordion_header").click(function (e) {
    if ($(this).hasClass("active")) {
        $(this).removeClass("active")
    } else {
        if (e.target.className != "down" && e.target.className != "up") {
            $(".accordion_header").removeClass("active");
            $(this).addClass("active");
        }
    }
});

$(function () {
    $('.up').on('click', function (e) {
        var wrapper = $(this).closest('.accordion_wrap')
        wrapper.insertBefore(wrapper.prev())
        $(".accordion_header").removeClass("active")
    })
    $('.down').on('click', function (e) {
        var wrapper = $(this).closest('.accordion_wrap')
        wrapper.insertAfter(wrapper.next())
        $(".accordion_header").removeClass("active")
    })
})

