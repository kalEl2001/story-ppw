$(".slider").click(function (e) {
    var element = document.body;
    element.classList.toggle("light-mode");
    var accordion = document.getElementsByClassName("accordion_header");
    for (let index = 0; index < accordion.length; index++) {
        accordion[index].classList.toggle("light-mode-header");
    }
    var accordionBody = document.getElementsByClassName("accordion_body");
    for (let index = 0; index < accordionBody.length; index++) {
        accordionBody[index].classList.toggle("light-mode-body");
    }
    var buttons = document.getElementsByClassName("btn");
    for (let index = 0; index < buttons.length; index++) {
        buttons[index].classList.toggle("light-mode-header");
    }
});