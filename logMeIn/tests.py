from django.test import TestCase

# Create your tests here.
from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve

from django.contrib.auth.models import User

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver import DesiredCapabilities
from selenium.webdriver.chrome.options import Options

from .models import *
from .views import *

import time
import os

from datetime import date


class Story10UnitTests(TestCase):

    def test_logMeIn_page_url_is_exist(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_logMeIn_page_is_using_logMeIn_function(self):
        found = resolve('/login/')
        self.assertEqual(found.func, logMeIn)

    def test_logMeIn_page_is_using_correct_html(self):
        response = Client().get('/login/')
        self.assertTemplateUsed(response, 'logMeIn.html')

    def test_signup_page_url_is_exist(self):
        response = Client().get('/login/signup/')
        self.assertTemplateUsed(response, 'signup.html')

    def test_signup_page_is_using_signup_function(self):
        found = resolve('/login/signup/')
        self.assertEqual(found.func, signup)


class Story10FuncTests(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.selenium = webdriver.Chrome(
            './chromedriver', chrome_options=chrome_options)
        self.selenium.get(self.live_server_url + '/login/')
        super(Story10FuncTests, self).setUp()

    def test_sign_up_log_out_wrong_password_and_log_in(self):
        selenium = self.selenium

        selenium.implicitly_wait(5)

        sign_up_link = selenium.find_element_by_id('sign-up')
        sign_up_link.click()

        selenium.implicitly_wait(5)

        username_textbox = selenium.find_element_by_id('id_username')
        password_textbox = selenium.find_element_by_id('id_password1')
        conf_password_textbox = selenium.find_element_by_id('id_password2')

        submit_button = selenium.find_element_by_id('submit-button')

        username_textbox.send_keys("test_user_1")
        password_textbox.send_keys("testPassword1")
        conf_password_textbox.send_keys("testPassword1")

        submit_button.click()

        selenium.implicitly_wait(5)

        logout_button = selenium.find_element_by_id('logout')
        logout_button.click()

        selenium.implicitly_wait(5)

        login_username_textbox = selenium.find_element_by_id('id_username')
        login_password_textbox = selenium.find_element_by_id('id_password')

        login_submit_button = selenium.find_element_by_id('submit')

        login_username_textbox.send_keys("test_user_1")
        login_password_textbox.send_keys("testWrongPassword")

        login_submit_button.click()

        selenium.implicitly_wait(5)

        login_username_textbox = selenium.find_element_by_id('id_username')
        login_password_textbox = selenium.find_element_by_id('id_password')

        login_username_textbox.clear()
        login_password_textbox.clear()

        login_submit_button = selenium.find_element_by_id('submit')

        login_username_textbox.send_keys("test_user_1")
        login_password_textbox.send_keys("testPassword1")

        login_submit_button.click()

        selenium.implicitly_wait(5)

        username_display = selenium.find_element_by_id('username-display').text

        self.assertEqual(username_display, "test_user_1")

    def test_sign_up_make_user_data_and_sign_in(self):
        selenium = self.selenium

        selenium.implicitly_wait(5)

        sign_up_link = selenium.find_element_by_id('sign-up')
        sign_up_link.click()

        selenium.implicitly_wait(5)

        username_textbox = selenium.find_element_by_id('id_username')
        password_textbox = selenium.find_element_by_id('id_password1')
        conf_password_textbox = selenium.find_element_by_id('id_password2')

        submit_button = selenium.find_element_by_id('submit-button')

        username_textbox.send_keys("test_user_1")
        password_textbox.send_keys("testPassword1")
        conf_password_textbox.send_keys("testPassword1")

        submit_button.click()

        selenium.implicitly_wait(5)

        logout_button = selenium.find_element_by_id('logout')
        logout_button.click()

        selenium.implicitly_wait(5)

        user = User.objects.all()[0]
        UserData.objects.create(user=user, first_name='Test', last_name='User',
                                photo='https://upload.wikimedia.org/wikipedia/commons/thumb/a/ad/Placeholder_no_text.svg/150px-Placeholder_no_text.svg.png', dob=date.today(), hobby="testing")

        self.assertEqual(str(UserData.objects.all()[0]), 'Test User')

        login_username_textbox = selenium.find_element_by_id('id_username')
        login_password_textbox = selenium.find_element_by_id('id_password')

        login_username_textbox.clear()
        login_password_textbox.clear()

        login_submit_button = selenium.find_element_by_id('submit')

        login_username_textbox.send_keys("test_user_1")
        login_password_textbox.send_keys("testPassword1")

        login_submit_button.click()

        selenium.implicitly_wait(5)

        selenium.get(self.live_server_url + '/')

        selenium.get(self.live_server_url + '/login/')

    def tearDown(self):
        self.selenium.quit()
        super(Story10FuncTests, self).tearDown()
