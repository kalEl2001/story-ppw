from django import forms


class loginForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'type': 'text',
        'title': 'Username',
        'placeholder': 'Plese enter your username',
        'required': True,
    }))
    password = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'type': 'password',
        'placeholder': 'Plese enter your password',
        'required': True,
    }))
