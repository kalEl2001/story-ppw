from django.db import models
from django.contrib.auth.models import User


class UserData(models.Model):
    user = models.OneToOneField(
        User, on_delete=models.CASCADE, primary_key=True)
    first_name = models.CharField(max_length=120)
    last_name = models.CharField(max_length=120)
    photo = models.CharField(max_length=350)
    dob = models.DateField()
    hobby = models.CharField(max_length=120)

    def __str__(self):
        return self.first_name + " " + self.last_name
