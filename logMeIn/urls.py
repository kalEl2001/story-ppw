from django.urls import path
from .views import *

urlpatterns = [
    path('', logMeIn, name='logMeIn'),
    path('logout/', logoutFunc, name='logout'),
    path('signup/', signup, name='signup'),
]
